/*
Navicat MySQL Data Transfer

Source Server         : MYSQL
Source Server Version : 80028
Source Host           : localhost:3306
Source Database       : restaurant

Target Server Type    : MYSQL
Target Server Version : 80028
File Encoding         : 65001

Date: 2022-06-13 20:07:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bills
-- ----------------------------
DROP TABLE IF EXISTS `bills`;
CREATE TABLE `bills` (
  `id_bill` int NOT NULL,
  `id_table` int NOT NULL,
  `id_member` int DEFAULT NULL,
  `time_order` datetime NOT NULL,
  `time_pay` datetime DEFAULT NULL,
  `money` int NOT NULL,
  PRIMARY KEY (`id_bill`),
  KEY `FK_bill_member` (`id_member`),
  KEY `FK_table_bill` (`id_table`),
  KEY `time_order` (`time_order`),
  CONSTRAINT `FK_bill_member` FOREIGN KEY (`id_member`) REFERENCES `member` (`id_member`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_table_bill` FOREIGN KEY (`id_table`) REFERENCES `tables` (`id_table`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of bills
-- ----------------------------
INSERT INTO `bills` VALUES ('1', '1', '1', '2022-06-01 21:21:22', '2022-06-01 22:21:22', '375');
INSERT INTO `bills` VALUES ('2', '3', null, '2022-06-01 21:21:22', '2022-06-01 22:43:59', '50');
INSERT INTO `bills` VALUES ('18', '2', null, '2022-06-02 20:36:45', null, '126');
INSERT INTO `bills` VALUES ('19', '1', null, '2022-06-02 20:37:51', null, '130');
INSERT INTO `bills` VALUES ('20', '4', null, '2022-06-01 20:38:58', null, '46');
INSERT INTO `bills` VALUES ('21', '3', null, '2022-06-02 20:40:43', null, '30');
INSERT INTO `bills` VALUES ('22', '2', null, '2022-06-02 20:41:41', null, '58');
INSERT INTO `bills` VALUES ('23', '1', null, '2022-06-02 20:52:17', null, '168');
INSERT INTO `bills` VALUES ('24', '4', null, '2022-06-03 21:15:34', '2022-06-03 21:15:43', '494');
INSERT INTO `bills` VALUES ('25', '2', null, '2022-06-03 21:16:56', null, '0');
INSERT INTO `bills` VALUES ('26', '4', null, '2022-06-03 21:17:19', '2022-06-03 21:17:23', '140');
INSERT INTO `bills` VALUES ('27', '4', null, '2022-06-03 21:18:06', null, '4');
INSERT INTO `bills` VALUES ('28', '3', null, '2022-06-03 21:18:37', '2022-06-03 21:18:41', '264');
INSERT INTO `bills` VALUES ('29', '3', null, '2022-06-03 21:19:16', '2022-06-03 21:19:21', '36');
INSERT INTO `bills` VALUES ('30', '3', null, '2022-06-08 21:19:39', '2022-06-08 21:19:42', '192');
INSERT INTO `bills` VALUES ('31', '1', null, '2022-06-08 21:31:42', '2022-06-08 21:31:50', '460');
INSERT INTO `bills` VALUES ('32', '3', null, '2021-04-08 22:41:34', null, '0');
INSERT INTO `bills` VALUES ('33', '3', null, '2021-04-15 19:33:02', null, '0');
INSERT INTO `bills` VALUES ('34', '4', null, '2021-04-08 19:45:49', '2022-06-08 19:46:38', '126');
INSERT INTO `bills` VALUES ('35', '2', null, '2022-06-08 19:55:06', null, '0');
INSERT INTO `bills` VALUES ('36', '1', null, '2022-06-08 19:56:44', null, '0');
INSERT INTO `bills` VALUES ('37', '4', null, '2022-06-08 19:57:37', null, '0');
INSERT INTO `bills` VALUES ('38', '3', null, '2022-06-08 19:58:23', null, '0');
INSERT INTO `bills` VALUES ('39', '4', null, '2022-06-08 19:58:32', null, '0');
INSERT INTO `bills` VALUES ('40', '2', null, '2022-06-08 19:58:48', '2022-06-10 19:59:23', '0');
INSERT INTO `bills` VALUES ('41', '2', null, '2022-06-10 20:04:06', '2022-06-10 20:04:09', '112');
INSERT INTO `bills` VALUES ('42', '2', null, '2022-06-10 20:04:19', '2022-06-10 20:04:23', '20');
INSERT INTO `bills` VALUES ('43', '2', null, '2022-06-10 20:24:41', null, '58');
INSERT INTO `bills` VALUES ('44', '1', null, '2022-06-10 20:34:26', null, '0');
INSERT INTO `bills` VALUES ('45', '3', null, '2022-06-10 20:34:41', '2022-06-10 20:34:46', '118');
INSERT INTO `bills` VALUES ('46', '4', null, '2022-06-11 20:35:42', '2022-06-11 20:35:46', '24');
INSERT INTO `bills` VALUES ('47', '3', null, '2022-06-11 20:36:56', '2022-06-11 20:36:58', '140');
INSERT INTO `bills` VALUES ('48', '4', null, '2022-06-11 20:37:39', '2022-06-11 20:37:41', '140');
INSERT INTO `bills` VALUES ('49', '2', null, '2022-06-11 20:37:59', '2022-06-11 20:38:02', '180');
INSERT INTO `bills` VALUES ('50', '3', null, '2022-06-11 20:39:00', '2022-06-11 20:39:04', '152');
INSERT INTO `bills` VALUES ('51', '3', null, '2022-06-11 20:39:30', '2022-06-11 20:39:32', '30');
INSERT INTO `bills` VALUES ('52', '2', null, '2022-06-11 20:40:21', '2022-06-11 20:40:26', '88');
INSERT INTO `bills` VALUES ('53', '3', null, '2022-06-11 15:25:20', '2022-06-11 15:42:30', '136');
INSERT INTO `bills` VALUES ('54', '2', null, '2022-06-12 12:33:43', '2022-06-12 12:33:48', '30');
INSERT INTO `bills` VALUES ('55', '2', null, '2022-06-12 21:41:48', null, '0');
INSERT INTO `bills` VALUES ('56', '4', null, '2022-06-12 21:45:24', null, '0');
INSERT INTO `bills` VALUES ('57', '3', null, '2022-06-12 21:48:58', null, '0');
INSERT INTO `bills` VALUES ('58', '1', null, '2022-06-12 21:50:48', null, '0');
INSERT INTO `bills` VALUES ('59', '4', null, '2022-06-12 21:51:11', null, '0');
INSERT INTO `bills` VALUES ('60', '3', null, '2022-06-12 21:55:12', null, '0');
INSERT INTO `bills` VALUES ('61', '2', null, '2022-06-13 16:20:53', null, '0');
INSERT INTO `bills` VALUES ('62', '4', null, '2022-06-13 16:22:17', '2022-06-13 16:46:39', '0');
INSERT INTO `bills` VALUES ('63', '3', null, '2022-06-13 16:23:26', '2022-06-13 16:46:29', '0');
INSERT INTO `bills` VALUES ('64', '1', null, '2022-06-13 16:24:00', '2022-06-13 16:46:43', '0');
INSERT INTO `bills` VALUES ('65', '3', null, '2022-06-13 16:26:17', '2022-06-13 16:46:47', '0');
INSERT INTO `bills` VALUES ('66', '4', null, '2022-06-13 16:27:02', '2022-06-13 16:46:51', '0');
INSERT INTO `bills` VALUES ('67', '2', null, '2022-06-13 16:27:48', '2022-06-13 16:46:55', '0');
INSERT INTO `bills` VALUES ('68', '4', null, '2022-06-13 16:33:40', '2022-06-13 16:35:41', '170');
INSERT INTO `bills` VALUES ('69', '4', null, '2022-06-13 16:36:02', '2022-06-13 16:36:30', '171');

-- ----------------------------
-- Table structure for bill_food
-- ----------------------------
DROP TABLE IF EXISTS `bill_food`;
CREATE TABLE `bill_food` (
  `id_food` int NOT NULL,
  `id_bill` int NOT NULL,
  `num` int NOT NULL,
  PRIMARY KEY (`id_food`,`id_bill`),
  KEY `FK_bill_food2` (`id_bill`),
  CONSTRAINT `FK_bill_food` FOREIGN KEY (`id_food`) REFERENCES `food` (`id_food`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_bill_food2` FOREIGN KEY (`id_bill`) REFERENCES `bills` (`id_bill`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of bill_food
-- ----------------------------
INSERT INTO `bill_food` VALUES ('1', '1', '2');
INSERT INTO `bill_food` VALUES ('1', '21', '1');
INSERT INTO `bill_food` VALUES ('1', '22', '1');
INSERT INTO `bill_food` VALUES ('1', '31', '3');
INSERT INTO `bill_food` VALUES ('1', '34', '1');
INSERT INTO `bill_food` VALUES ('1', '43', '1');
INSERT INTO `bill_food` VALUES ('1', '49', '6');
INSERT INTO `bill_food` VALUES ('1', '51', '1');
INSERT INTO `bill_food` VALUES ('1', '53', '1');
INSERT INTO `bill_food` VALUES ('1', '54', '1');
INSERT INTO `bill_food` VALUES ('1', '68', '1');
INSERT INTO `bill_food` VALUES ('2', '20', '1');
INSERT INTO `bill_food` VALUES ('2', '22', '1');
INSERT INTO `bill_food` VALUES ('2', '23', '6');
INSERT INTO `bill_food` VALUES ('2', '26', '5');
INSERT INTO `bill_food` VALUES ('2', '41', '4');
INSERT INTO `bill_food` VALUES ('2', '43', '1');
INSERT INTO `bill_food` VALUES ('2', '45', '4');
INSERT INTO `bill_food` VALUES ('2', '47', '5');
INSERT INTO `bill_food` VALUES ('2', '48', '5');
INSERT INTO `bill_food` VALUES ('2', '69', '1');
INSERT INTO `bill_food` VALUES ('3', '1', '1');
INSERT INTO `bill_food` VALUES ('3', '24', '1');
INSERT INTO `bill_food` VALUES ('3', '30', '4');
INSERT INTO `bill_food` VALUES ('3', '34', '2');
INSERT INTO `bill_food` VALUES ('3', '68', '1');
INSERT INTO `bill_food` VALUES ('3', '69', '1');
INSERT INTO `bill_food` VALUES ('4', '1', '1');
INSERT INTO `bill_food` VALUES ('4', '18', '1');
INSERT INTO `bill_food` VALUES ('4', '19', '1');
INSERT INTO `bill_food` VALUES ('4', '24', '2');
INSERT INTO `bill_food` VALUES ('4', '50', '4');
INSERT INTO `bill_food` VALUES ('5', '1', '2');
INSERT INTO `bill_food` VALUES ('5', '18', '1');
INSERT INTO `bill_food` VALUES ('5', '19', '1');
INSERT INTO `bill_food` VALUES ('5', '24', '4');
INSERT INTO `bill_food` VALUES ('5', '28', '3');
INSERT INTO `bill_food` VALUES ('5', '31', '4');
INSERT INTO `bill_food` VALUES ('5', '52', '1');
INSERT INTO `bill_food` VALUES ('5', '53', '1');
INSERT INTO `bill_food` VALUES ('5', '68', '1');
INSERT INTO `bill_food` VALUES ('5', '69', '1');
INSERT INTO `bill_food` VALUES ('6', '29', '6');
INSERT INTO `bill_food` VALUES ('6', '45', '2');
INSERT INTO `bill_food` VALUES ('6', '69', '1');
INSERT INTO `bill_food` VALUES ('7', '19', '1');
INSERT INTO `bill_food` VALUES ('7', '27', '1');
INSERT INTO `bill_food` VALUES ('7', '42', '5');
INSERT INTO `bill_food` VALUES ('7', '46', '6');
INSERT INTO `bill_food` VALUES ('7', '68', '1');
INSERT INTO `bill_food` VALUES ('7', '69', '1');
INSERT INTO `bill_food` VALUES ('8', '20', '1');
INSERT INTO `bill_food` VALUES ('8', '24', '1');
INSERT INTO `bill_food` VALUES ('8', '29', '1');
INSERT INTO `bill_food` VALUES ('8', '31', '1');
INSERT INTO `bill_food` VALUES ('8', '53', '1');

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `category` char(20) NOT NULL,
  PRIMARY KEY (`category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('川菜');
INSERT INTO `categories` VALUES ('湘菜');
INSERT INTO `categories` VALUES ('甜品');
INSERT INTO `categories` VALUES ('粤菜');

-- ----------------------------
-- Table structure for discounts
-- ----------------------------
DROP TABLE IF EXISTS `discounts`;
CREATE TABLE `discounts` (
  `id_discount` int NOT NULL,
  `off_price` int NOT NULL,
  `require_points` int NOT NULL,
  PRIMARY KEY (`id_discount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of discounts
-- ----------------------------
INSERT INTO `discounts` VALUES ('1', '5', '150');
INSERT INTO `discounts` VALUES ('2', '10', '250');
INSERT INTO `discounts` VALUES ('3', '20', '450');

-- ----------------------------
-- Table structure for emp
-- ----------------------------
DROP TABLE IF EXISTS `emp`;
CREATE TABLE `emp` (
  `id_emp` int NOT NULL,
  `id_server` int DEFAULT NULL,
  `name_emp` char(20) NOT NULL,
  `sex_emp` char(1) DEFAULT NULL,
  `phone_num` char(11) DEFAULT NULL,
  `position` char(20) DEFAULT NULL,
  PRIMARY KEY (`id_emp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of emp
-- ----------------------------
INSERT INTO `emp` VALUES ('1', '1', 'Moly', 'M', '13345677654', 'server');
INSERT INTO `emp` VALUES ('2', '2', 'Jess', 'M', '13645677654', 'server');
INSERT INTO `emp` VALUES ('3', null, '张三', 'M', '13222222222', 'cook');

-- ----------------------------
-- Table structure for food
-- ----------------------------
DROP TABLE IF EXISTS `food`;
CREATE TABLE `food` (
  `id_food` int NOT NULL,
  `category` char(20) NOT NULL,
  `name_food` char(20) NOT NULL,
  `introduction` char(100) DEFAULT NULL,
  `price` int NOT NULL,
  `url` char(100) DEFAULT NULL,
  PRIMARY KEY (`id_food`),
  KEY `FK_food_category` (`category`),
  CONSTRAINT `FK_food_category` FOREIGN KEY (`category`) REFERENCES `categories` (`category`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of food
-- ----------------------------
INSERT INTO `food` VALUES ('1', '川菜', '麻婆豆腐', null, '30', null);
INSERT INTO `food` VALUES ('2', '川菜', '麻辣相干', null, '28', null);
INSERT INTO `food` VALUES ('3', '湘菜', '小炒肉', null, '48', null);
INSERT INTO `food` VALUES ('4', '粤菜', '白切鸡', null, '48', null);
INSERT INTO `food` VALUES ('5', '粤菜', '鱼子酱沙拉', null, '88', null);
INSERT INTO `food` VALUES ('6', '甜品', '可口可乐', null, '3', null);
INSERT INTO `food` VALUES ('7', '甜品', '维维豆奶', null, '4', null);
INSERT INTO `food` VALUES ('8', '甜品', '老八小汉堡', null, '18', null);

-- ----------------------------
-- Table structure for member
-- ----------------------------
DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `id_member` int NOT NULL,
  `name_member` char(20) DEFAULT NULL,
  `points` int NOT NULL,
  `sex` char(1) DEFAULT NULL,
  `phone_num` char(11) DEFAULT NULL,
  PRIMARY KEY (`id_member`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of member
-- ----------------------------
INSERT INTO `member` VALUES ('1', 'SHAB', '790', null, null);
INSERT INTO `member` VALUES ('2', '阿萨', '191', 'm', '1235');
INSERT INTO `member` VALUES ('3', null, '30', null, null);
INSERT INTO `member` VALUES ('4', null, '112', null, null);

-- ----------------------------
-- Table structure for member_point_bill
-- ----------------------------
DROP TABLE IF EXISTS `member_point_bill`;
CREATE TABLE `member_point_bill` (
  `id_point_bill` int NOT NULL,
  `id_member` int NOT NULL,
  `time_point` datetime NOT NULL,
  `point` int NOT NULL,
  `note` char(20) DEFAULT NULL,
  PRIMARY KEY (`id_point_bill`),
  KEY `FK_member_point_bill` (`id_member`),
  CONSTRAINT `FK_member_point_bill` FOREIGN KEY (`id_member`) REFERENCES `member` (`id_member`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of member_point_bill
-- ----------------------------
INSERT INTO `member_point_bill` VALUES ('1', '1', '2022-06-13 17:37:24', '0', null);

-- ----------------------------
-- Table structure for servers
-- ----------------------------
DROP TABLE IF EXISTS `servers`;
CREATE TABLE `servers` (
  `id_server` int NOT NULL,
  `id_emp` int NOT NULL,
  PRIMARY KEY (`id_server`),
  KEY `FK_to_server` (`id_emp`),
  CONSTRAINT `FK_to_server` FOREIGN KEY (`id_emp`) REFERENCES `emp` (`id_emp`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of servers
-- ----------------------------
INSERT INTO `servers` VALUES ('1', '1');
INSERT INTO `servers` VALUES ('2', '2');

-- ----------------------------
-- Table structure for tables
-- ----------------------------
DROP TABLE IF EXISTS `tables`;
CREATE TABLE `tables` (
  `id_table` int NOT NULL,
  `id_server` int NOT NULL,
  `num_people` int NOT NULL,
  `id_bill` int DEFAULT NULL,
  `id_member` int DEFAULT NULL,
  PRIMARY KEY (`id_table`),
  KEY `FK_server_table` (`id_server`),
  CONSTRAINT `FK_server_table` FOREIGN KEY (`id_server`) REFERENCES `servers` (`id_server`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of tables
-- ----------------------------
INSERT INTO `tables` VALUES ('1', '1', '4', null, null);
INSERT INTO `tables` VALUES ('2', '1', '4', null, null);
INSERT INTO `tables` VALUES ('3', '2', '2', null, null);
INSERT INTO `tables` VALUES ('4', '2', '8', null, null);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_position` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('0001', '123456', 'long', '老板');
INSERT INTO `user` VALUES ('0002', '123567', null, '');

-- ----------------------------
-- View structure for food_sales
-- ----------------------------
DROP VIEW IF EXISTS `food_sales`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `food_sales` AS select `food`.`name_food` AS `name_food`,sum(`bill_food`.`num`) AS `sum(num)` from (`bill_food` join `food`) where (`food`.`id_food` = `bill_food`.`id_food`) group by `food`.`id_food` order by sum(`bill_food`.`num`) desc ;

-- ----------------------------
-- View structure for food_time_sales
-- ----------------------------
DROP VIEW IF EXISTS `food_time_sales`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `food_time_sales` AS select sum(`bills`.`money`) AS `sum(money)` from `bills` group by `bills`.`time_pay` order by sum(`bills`.`money`) ;

-- ----------------------------
-- View structure for sales_time
-- ----------------------------
DROP VIEW IF EXISTS `sales_time`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sales_time` AS select sum(`bills`.`money`) AS `sum(money)` from `bills` where (`bills`.`time_pay` between '2020.01.01' and '2021.01.01') ;
