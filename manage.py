
import database_manage
from tkinter import messagebox
#import main
import tkinter as tk  # 使用Tkinter前需要先导入
from database_manage import *
from tkinter import *

def show():

    window = tk.Toplevel()
    window['bg'] = '#d0c0c0'
    window.title('后台页面')
    window.geometry('250x250')

    def inquire_menu():
        window_menu = tk.Toplevel()
        window_menu['bg'] = '#d0c0c0'
        window_menu.title('所有菜品页面')
        window_menu.geometry('220x230')

        food = {}
        food=database_manage.db_get_food()
        var = tk.StringVar()
        listbox = tk.Listbox(window_menu, listvariable=food)
        #listbox.Text(window,wigth=100,height=300)
        listbox.grid(row=0, column=6,ipadx=30,ipady=10,columnspan=5,rowspan=5)
        listbox.insert("end", "id:  "+" 类别： "+" 名称："+" 价格：")
        for food_item in food:
            #listbox.insert("end", food[food_item][0])
            listbox.insert("end", food[food_item][0]+"    "+food[food_item][1]+"    "+food[food_item][2]+"    "+food[food_item][3])


    def add_menu():
        window_add = tk.Toplevel()
        window_add['bg'] = '#d0c0c0'
        window_add.title('添加菜品页面')
        window_add.geometry('300x200')

        Label(window_add, text='id_food').grid(row=1, column=0)
        id = Entry(window_add)
        id.grid(row=1, column=1)
        Label(window_add, text='category').grid(row=2, column=0)
        category = Entry(window_add)
        category.grid(row=2, column=1)

        Label(window_add, text='name').grid(row=3, column=0)
        name = Entry(window_add)
        name.grid(row=3, column=1)
        Label(window_add, text='price').grid(row=4, column=0)
        price = Entry(window_add)
        price.grid(row=4, column=1)



        def add():
            falg=db_get_all_categories(category)
            if(falg==1):
                ret=db_get_add(id,category,name,price)
                if(ret==1):
                    messagebox.showinfo(title='successful', message='添加成功')
                else:
                    messagebox.showinfo(title='失败', message='由于food表的外键约束，不能在pycharm里用语句添加')
            else:
                messagebox.showinfo(title='失败', message='category错误')

        Button(window_add, text='添加', command=add).grid(row=6, column=2, columnspan=2)


    def alter_menu():
        window_alter = tk.Toplevel()
        window_alter['bg'] = '#d0c0c0'
        window_alter.title('修改菜品页面')
        window_alter.geometry('300x200')

        Label(window_alter, text='菜品名称').grid(row=1, column=0)
        name = Entry(window_alter)
        name.grid(row=1, column=1)
        Label(window_alter, text='菜品价格').grid(row=2, column=0)
        price = Entry(window_alter)
        price.grid(row=2, column=1)


        def alters():
            falg = db_alter(name,price)
            if falg == 1:
                messagebox.showinfo(title='successful', message='修改成功')
            else:
                messagebox.showinfo(title='失败', message='修改失败')

        Button(window_alter, text='修改', command=alters).grid(row=6, column=2, columnspan=2)


    def delete_menu():
        window_delete = tk.Toplevel()
        window_delete['bg'] = '#d0c0c0'
        window_delete.title('删除菜品页面')
        window_delete.geometry('300x200')

        Label(window_delete, text='菜品名称').grid(row=1, column=0)
        name = Entry(window_delete)
        name.grid(row=1, column=1)

        def deletes():
            falg = db_delete(name)

            if falg == 1:
                messagebox.showinfo(title='successful', message='删除成功')
            else:
                messagebox.showinfo(title='失败', message='由于food表的外键约束，不能在pycharm里用语句删除')

        Button(window_delete, text='删除', command=deletes).grid(row=6, column=2, columnspan=2)

    def add_member():
        window_addm = tk.Toplevel()
        window_addm['bg'] = '#d0c0c0'
        window_addm.title('增加会员页面')
        window_addm.geometry('300x200')

        Label(window_addm, text='id_member').grid(row=0, column=0)
        member = Entry(window_addm)
        member.grid(row=0, column=1)
        Label(window_addm, text='name').grid(row=1, column=0)
        name = Entry(window_addm)
        name.grid(row=1, column=1)
        Label(window_addm, text='sex').grid(row=2, column=0)
        sex = Entry(window_addm)
        sex.grid(row=2, column=1)
        Label(window_addm, text='phone').grid(row=3, column=0)
        phone = Entry(window_addm)
        phone.grid(row=3, column=1)

        def adds():
            falg = db_add_member(member,name,sex,phone)
            if falg == 1:
                messagebox.showinfo(title='successful', message='增加成功')
            else:
                messagebox.showinfo(title='失败', message='增加失败')

        Button(window_addm, text='增加', command=adds).grid(row=6, column=2, columnspan=2)

    tk.Button(window, text='查询所有菜品', width=15, height=2,command=inquire_menu).grid(row=0, column=1)
    tk.Button(window, text='添加菜品', width=15, height=2,command=add_menu).grid(row=1, column=1)
    tk.Button(window, text='修改菜品价格', width=15, height=2,command=alter_menu).grid(row=2, column=1)
    tk.Button(window, text='删除菜品', width=15, height=2,command=delete_menu).grid(row=3, column=1)
    tk.Button(window, text='增加会员', width=15, height=2, command=add_member).grid(row=4, column=1)


    window.mainloop()
